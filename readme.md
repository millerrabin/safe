# Safe-ops
The set of functions for safe operation in js 


## How to install
```bash
    npm install safe-ops
```    

## Usage

### isEmpty(string)
Checks string for null, undefined and ''

### regExp(text, flags)
Safe create RegExp object from text. Special symbols will be masked.

### compare(source, fobj)
Compares source object with fields from fobj;
if source object contains all fields from fobj with same values when returns true, 
otherwise returns false;
  