function isEmpty(value) {
    return (value == null) || (value == '');
}

function regExp(text, flags) {
    const maskedRegStr = text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    return new RegExp(maskedRegStr, flags);
}

function compare(source, fobj) {
    for (const key in fobj) {
        if (!fobj.hasOwnProperty(key)) continue;
        const sVal = source[key];
        const tVal = fobj[key];
        if (sVal != tVal) return false;
    }
    return true;
}

export default { isEmpty, regExp, compare }